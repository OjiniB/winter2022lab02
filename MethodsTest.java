public class MethodsTest
{
  public static void methodNoInputNoReturn()
  {
    int x = 50;
    System.out.println("I am in a method that takes no input and returns nothing");  
    System.out.println(x);
  }
  
    public static void methodOneInputNoReturn(int v)
  {
    v = 0;
    System.out.println("Inside the method one input no return");
    System.out.println(v);    
  }
    
     public static void methodTwoInputNoReturn(int o, double m)
  {
    System.out.println("Inside the method two input no return");
  }
    
     public static int methodNoInputReturnInt()
  {
    System.out.print("Inside the method No input RETURN an int = ");
    return 6;
  }
     
       public static double sumSquareRoot(int p, int d)
  {
    int addTwoInts = p + d;
    double resultTwoInts = Math.sqrt((addTwoInts));
    System.out.print("method sum of the Square Root is : ");
    return resultTwoInts ;
  }
    public static void main(String[] args)
  { 
    int x = 10;
    System.out.println(x);
    methodNoInputNoReturn();
    System.out.println(x);
	
    methodOneInputNoReturn(5);
    methodOneInputNoReturn(x);
    methodOneInputNoReturn(3 + 4);
    methodOneInputNoReturn(10); 
    methodOneInputNoReturn(x); 
    methodOneInputNoReturn(x + 50); 
	
    methodTwoInputNoReturn(7 , 25.8);
	
    int  j = methodNoInputReturnInt();
    System.out.println(j);
    double sumResult = sumSquareRoot(6 , 3);
    System.out.println(sumResult);
	
	String s1 = "hello";
	String s2 = "goodbye";
	
	System.out.println("the length of hello word is " + s1.length());
    System.out.println("the length of goodbye word is " + s2.length());
	
	System.out.println("with add One STATIC method = " + SecondClass.addOne(50));
    SecondClass sc = new SecondClass();
    System.out.println("with add Two INSTANCE method = " + sc.addTwo(50));
	}
	
}







