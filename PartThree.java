import java.util.Scanner; 
public class PartThree
{
  
  public static void main(String[] args)
  { 
    Scanner geometric = new Scanner(System.in);
    System.out.println("Please enter the LENGTH of a SQUARE"); 
    int lengthSqr = geometric.nextInt();
    System.out.println("The AREA of a SQUARE is : " +  AreaComputations.areaSquare(lengthSqr));
    System.out.println("\nPlease enter the LENGTH of a RECTANGLE"); 
    int lengthRec = geometric.nextInt();
    AreaComputations ac = new AreaComputations();
    System.out.println("Please enter the WIDTH of a RECTANGLE"); 
    int widthRec = geometric.nextInt();
    System.out.println("The AREA of a RECTANGLE is : " + ac.areaRectangle(lengthRec,widthRec)); 
  }
  
}

