public class AreaComputations {
  
  public static int areaSquare(int sideSqr)
  {
    int areaOfSquare = sideSqr * sideSqr;
    return areaOfSquare;
  }
  
  public int areaRectangle(int lengthRec, int widthRec)
  {
    int areaOfRectangle = lengthRec * widthRec;
    return areaOfRectangle;
  }
   
}

